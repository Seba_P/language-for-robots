var searchData=
[
  ['lang4robots_5fexecutecommand',['lang4robots_executeCommand',['../lang4robots_8c.html#a050ff49b963f6e0affc599f66acbe027',1,'lang4robots_executeCommand(uint8_t comm, uint32_t param):&#160;lang4robots.c'],['../lang4robots_8h.html#a050ff49b963f6e0affc599f66acbe027',1,'lang4robots_executeCommand(uint8_t comm, uint32_t param):&#160;lang4robots.c']]],
  ['lang4robots_5finit',['lang4robots_init',['../lang4robots_8c.html#ac9039be82b9f87abf33eea8db561438d',1,'lang4robots_init(void):&#160;lang4robots.c'],['../lang4robots_8h.html#ac9039be82b9f87abf33eea8db561438d',1,'lang4robots_init(void):&#160;lang4robots.c']]],
  ['lang4robots_5freceivecommand',['lang4robots_receiveCommand',['../lang4robots_8c.html#aeb171ac11445cfb7cfaf1012742c60b8',1,'lang4robots_receiveCommand(uint8_t dataPipe):&#160;lang4robots.c'],['../lang4robots_8h.html#aeb171ac11445cfb7cfaf1012742c60b8',1,'lang4robots_receiveCommand(uint8_t dataPipe):&#160;lang4robots.c']]],
  ['lang4robots_5fsendcommand',['lang4robots_sendCommand',['../lang4robots_8c.html#a6e10a11c5c0c4eaa3a9cdea1303c3323',1,'lang4robots_sendCommand(uint8_t *addr, uint8_t comm):&#160;lang4robots.c'],['../lang4robots_8h.html#a6e10a11c5c0c4eaa3a9cdea1303c3323',1,'lang4robots_sendCommand(uint8_t *addr, uint8_t comm):&#160;lang4robots.c']]]
];
