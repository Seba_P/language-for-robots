var searchData=
[
  ['spi1_5fbusy',['spi1_busy',['../_s_p_i_8c.html#a5937a8541de457b69abac28cd10bf1fc',1,'spi1_busy():&#160;SPI.c'],['../_s_p_i_8h.html#a5937a8541de457b69abac28cd10bf1fc',1,'spi1_busy():&#160;SPI.c']]],
  ['spi1_5fdata_5fs_5fpointer',['spi1_data_s_pointer',['../_s_p_i_8c.html#aeae8e549a1e261971f271d24fca08951',1,'spi1_data_s_pointer():&#160;SPI.c'],['../_s_p_i_8h.html#aeae8e549a1e261971f271d24fca08951',1,'spi1_data_s_pointer():&#160;SPI.c']]],
  ['spi1_5fdata_5fs_5fsize',['spi1_data_s_size',['../_s_p_i_8c.html#a693cedf90117bd8e6b7c57e898e860cb',1,'spi1_data_s_size():&#160;SPI.c'],['../_s_p_i_8h.html#a693cedf90117bd8e6b7c57e898e860cb',1,'spi1_data_s_size():&#160;SPI.c']]],
  ['spi1_5freceive_5fbuffer',['spi1_receive_buffer',['../_s_p_i_8c.html#a094d036b7e71611e147b72c8868e543e',1,'spi1_receive_buffer():&#160;SPI.c'],['../_s_p_i_8h.html#a094d036b7e71611e147b72c8868e543e',1,'spi1_receive_buffer():&#160;SPI.c']]],
  ['spi1_5freceive_5fbuffer_5fcounter',['spi1_receive_buffer_counter',['../_s_p_i_8c.html#a8a3dd92870c8e89ae7936ef6fc3338e6',1,'spi1_receive_buffer_counter():&#160;SPI.c'],['../_s_p_i_8h.html#a8a3dd92870c8e89ae7936ef6fc3338e6',1,'spi1_receive_buffer_counter():&#160;SPI.c']]],
  ['spi1_5freceive_5fbuffer_5fsize',['spi1_receive_buffer_size',['../_s_p_i_8c.html#a60a60d88a808e74a2576d1428af60960',1,'spi1_receive_buffer_size():&#160;SPI.c'],['../_s_p_i_8h.html#a60a60d88a808e74a2576d1428af60960',1,'spi1_receive_buffer_size():&#160;SPI.c']]],
  ['spi1_5frecive_5fmode',['spi1_recive_mode',['../_s_p_i_8c.html#a89732086e2c65340b07fca90d68e2a75',1,'SPI.c']]]
];
