var searchData=
[
  ['setup_5faw',['SETUP_AW',['../n_r_f24_l01_p_8h.html#af5ef355ba3eca336db1285cab353ddc2',1,'SETUP_AW():&#160;nRF24L01P.h'],['../n_r_f24_l01_p__old_8h.html#af5ef355ba3eca336db1285cab353ddc2',1,'SETUP_AW():&#160;nRF24L01P_old.h']]],
  ['setup_5fretr',['SETUP_RETR',['../n_r_f24_l01_p_8h.html#a2188309b3eceeae158dd64109cd919aa',1,'SETUP_RETR():&#160;nRF24L01P.h'],['../n_r_f24_l01_p__old_8h.html#a2188309b3eceeae158dd64109cd919aa',1,'SETUP_RETR():&#160;nRF24L01P_old.h']]],
  ['sim_5fscgc5_5fclear',['SIM_SCGC5_CLEAR',['../pin_management_8h.html#a995b18d78b50fc1042399d88e0d284c5',1,'pinManagement.h']]],
  ['sim_5fscgc5_5fglue',['SIM_SCGC5_GLUE',['../pin_management_8h.html#a00d265220d30030bf43a1e21e936a674',1,'pinManagement.h']]],
  ['sim_5fscgc5_5fset',['SIM_SCGC5_SET',['../pin_management_8h.html#a88cf51c361652e2c5d91a63b60d394b1',1,'pinManagement.h']]],
  ['slcd_2ec',['slcd.c',['../slcd_8c.html',1,'']]],
  ['slcd_2eh',['slcd.h',['../slcd_8h.html',1,'']]],
  ['slcdclear',['slcdClear',['../slcd_8c.html#aaaff4e04850c66e370264dc74d0964cc',1,'slcdClear(void):&#160;slcd.c'],['../slcd_8h.html#aaaff4e04850c66e370264dc74d0964cc',1,'slcdClear(void):&#160;slcd.c']]],
  ['slcddemo',['slcdDemo',['../slcd_8c.html#ae2a224bcd35d271a7ea6615efc71b144',1,'slcdDemo(void):&#160;slcd.c'],['../slcd_8h.html#ae2a224bcd35d271a7ea6615efc71b144',1,'slcdDemo(void):&#160;slcd.c']]],
  ['slcddisplay',['slcdDisplay',['../slcd_8c.html#ace28f520bc05c0f3f359dffa31d9ffe9',1,'slcdDisplay(uint16_t value, uint16_t format):&#160;slcd.c'],['../slcd_8h.html#ace28f520bc05c0f3f359dffa31d9ffe9',1,'slcdDisplay(uint16_t value, uint16_t format):&#160;slcd.c']]],
  ['slcderr',['slcdErr',['../slcd_8c.html#a9a69ca2f6badebd7740c836eef1d2b56',1,'slcdErr(uint8_t number):&#160;slcd.c'],['../slcd_8h.html#a9a69ca2f6badebd7740c836eef1d2b56',1,'slcdErr(uint8_t number):&#160;slcd.c']]],
  ['slcdinitialize',['slcdInitialize',['../slcd_8c.html#a2b629aa8c70eb086a7a4e61a8d074f13',1,'slcdInitialize(void):&#160;slcd.c'],['../slcd_8h.html#a2b629aa8c70eb086a7a4e61a8d074f13',1,'slcdInitialize(void):&#160;slcd.c']]],
  ['slcdset',['slcdSet',['../slcd_8c.html#a6c8dfaa32f3473330d766defea9a51fd',1,'slcdSet(uint8_t value, uint8_t digit):&#160;slcd.c'],['../slcd_8h.html#a6c8dfaa32f3473330d766defea9a51fd',1,'slcdSet(uint8_t value, uint8_t digit):&#160;slcd.c']]],
  ['spi',['SPI',['../_s_p_i_8h.html#ad461bf888d2ce3175bec549d9f3bc94e',1,'SPI.h']]],
  ['spi_2ec',['SPI.c',['../_s_p_i_8c.html',1,'']]],
  ['spi_2eh',['SPI.h',['../_s_p_i_8h.html',1,'']]],
  ['spi0_5fbyte_5fsend',['spi0_byte_send',['../_s_p_i_8h.html#aeb619a69d7a7df5cefa9f3da353244e5',1,'SPI.h']]],
  ['spi0_5fclock_5fphase',['SPI0_CLOCK_PHASE',['../_s_p_i_8h.html#a6ed70547f44626b62feb64331d3aedd8',1,'SPI.h']]],
  ['spi0_5fclock_5fpolarity',['SPI0_CLOCK_POLARITY',['../_s_p_i_8h.html#a555dfc68c16406ca4f8b4d30829e30e8',1,'SPI.h']]],
  ['spi0_5fdata_5flength',['SPI0_DATA_LENGTH',['../_s_p_i_8h.html#aed12daf303a34e2485ab6ba497e70d87',1,'SPI.h']]],
  ['spi0_5fdma_5fmode',['SPI0_DMA_MODE',['../_s_p_i_8h.html#aa92f93239ee677358be9ee21015369dd',1,'SPI.h']]],
  ['spi0_5ffirst_5fbit',['SPI0_FIRST_BIT',['../_s_p_i_8h.html#a000ce64b6a970bcf3534951e6e9f0595',1,'SPI.h']]],
  ['spi0_5fmiso',['SPI0_MISO',['../_s_p_i_8h.html#aabba8137ea69a9180bedb49e83873988',1,'SPI.h']]],
  ['spi0_5fmiso_5fmux',['SPI0_MISO_MUX',['../_s_p_i_8h.html#a00a3076626c0136cecb21631bbd76a27',1,'SPI.h']]],
  ['spi0_5fmiso_5fport',['SPI0_MISO_PORT',['../_s_p_i_8h.html#a367efecfb76b43306039c8a71422db8a',1,'SPI.h']]],
  ['spi0_5fmosi',['SPI0_MOSI',['../_s_p_i_8h.html#a16ce69be83444ff939653090fb7fa151',1,'SPI.h']]],
  ['spi0_5fmosi_5fmux',['SPI0_MOSI_MUX',['../_s_p_i_8h.html#a7f685a7c2a58e20fda44684783752a4a',1,'SPI.h']]],
  ['spi0_5fmosi_5fport',['SPI0_MOSI_PORT',['../_s_p_i_8h.html#adf6280e42a22140303cfae16f7e01e8d',1,'SPI.h']]],
  ['spi0_5fms_5fmode',['SPI0_MS_MODE',['../_s_p_i_8h.html#abaa0c3d0dcdebcd5aca2d32cbd37e629',1,'SPI.h']]],
  ['spi0_5fpcs0',['SPI0_PCS0',['../_s_p_i_8h.html#adec51e7b3b1e255560898cc3580d462e',1,'SPI.h']]],
  ['spi0_5fpcs0_5fmux',['SPI0_PCS0_MUX',['../_s_p_i_8h.html#a57b511018cc6a5039d22cf3b4e65b0af',1,'SPI.h']]],
  ['spi0_5fpcs0_5fport',['SPI0_PCS0_PORT',['../_s_p_i_8h.html#a7aa6a81a1d7c42e5a543f2cad678b3b4',1,'SPI.h']]],
  ['spi0_5fsck',['SPI0_SCK',['../_s_p_i_8h.html#a85a885041758879c90a08fed7a75d2e2',1,'SPI.h']]],
  ['spi0_5fsck_5fmux',['SPI0_SCK_MUX',['../_s_p_i_8h.html#a5faff69e3fd08d842c675ceb67ad0be5',1,'SPI.h']]],
  ['spi0_5fsck_5fport',['SPI0_SCK_PORT',['../_s_p_i_8h.html#a89c39ef9bb3c344f11fbc6840095a8c8',1,'SPI.h']]],
  ['spi0_5fset_5fmatch_5fvalue',['spi0_set_match_value',['../_s_p_i_8c.html#abc9574979e282bfab80ddac4d50e5a2e',1,'spi0_set_match_value(uint8_t ML, uint8_t MH):&#160;SPI.c'],['../_s_p_i_8h.html#abc9574979e282bfab80ddac4d50e5a2e',1,'spi0_set_match_value(uint8_t ML, uint8_t MH):&#160;SPI.c']]],
  ['spi0_5fset_5fmatch_5fvalue_5fml',['spi0_set_match_value_ML',['../_s_p_i_8c.html#a778d0cfec30eee9b8790e6f180abc66c',1,'spi0_set_match_value_ML(uint8_t ML):&#160;SPI.c'],['../_s_p_i_8h.html#a778d0cfec30eee9b8790e6f180abc66c',1,'spi0_set_match_value_ML(uint8_t ML):&#160;SPI.c']]],
  ['spi0_5fsppr',['SPI0_SPPR',['../_s_p_i_8h.html#ac74490e06c6f3c129c962db0d30746b2',1,'SPI.h']]],
  ['spi0_5fspr',['SPI0_SPR',['../_s_p_i_8h.html#a4ca789103aadb86ecc71157a126ce46a',1,'SPI.h']]],
  ['spi0_5ftransfer_5fmode',['SPI0_TRANSFER_MODE',['../_s_p_i_8h.html#a1eb5506a7fb7bd77999469ba1130fd45',1,'SPI.h']]],
  ['spi0init',['spi0init',['../_s_p_i_8h.html#a3a755190f64d26b666d115e30439caea',1,'SPI.h']]],
  ['spi1_5fbusy',['spi1_busy',['../_s_p_i_8c.html#a5937a8541de457b69abac28cd10bf1fc',1,'spi1_busy():&#160;SPI.c'],['../_s_p_i_8h.html#a5937a8541de457b69abac28cd10bf1fc',1,'spi1_busy():&#160;SPI.c']]],
  ['spi1_5fbyte_5fsend',['spi1_byte_send',['../_s_p_i_8c.html#a6689cb079c9e7a6ea2b4d7968ae4029b',1,'spi1_byte_send(uint8_t data, _Bool receive):&#160;SPI.c'],['../_s_p_i_8h.html#a6689cb079c9e7a6ea2b4d7968ae4029b',1,'spi1_byte_send(uint8_t data, _Bool receive):&#160;SPI.c']]],
  ['spi1_5fclock_5fphase',['SPI1_CLOCK_PHASE',['../_s_p_i_8h.html#af1f77a28787c5bfc2cd5ac609a133191',1,'SPI.h']]],
  ['spi1_5fclock_5fpolarity',['SPI1_CLOCK_POLARITY',['../_s_p_i_8h.html#a8e681c8fbe784728b82e2a1a48034854',1,'SPI.h']]],
  ['spi1_5fdata_5flength',['SPI1_DATA_LENGTH',['../_s_p_i_8h.html#a07642c7429f5358109b087c070ad4fa7',1,'SPI.h']]],
  ['spi1_5fdata_5fs_5fpointer',['spi1_data_s_pointer',['../_s_p_i_8c.html#aeae8e549a1e261971f271d24fca08951',1,'spi1_data_s_pointer():&#160;SPI.c'],['../_s_p_i_8h.html#aeae8e549a1e261971f271d24fca08951',1,'spi1_data_s_pointer():&#160;SPI.c']]],
  ['spi1_5fdata_5fs_5fsize',['spi1_data_s_size',['../_s_p_i_8c.html#a693cedf90117bd8e6b7c57e898e860cb',1,'spi1_data_s_size():&#160;SPI.c'],['../_s_p_i_8h.html#a693cedf90117bd8e6b7c57e898e860cb',1,'spi1_data_s_size():&#160;SPI.c']]],
  ['spi1_5fdata_5fsend',['spi1_data_send',['../_s_p_i_8h.html#ab8ae59cdc1b359e7fcba46f406543cde',1,'SPI.h']]],
  ['spi1_5fdma_5fmode',['SPI1_DMA_MODE',['../_s_p_i_8h.html#acebbfdcb30df27ccf9e5e98f5a93c7d4',1,'SPI.h']]],
  ['spi1_5ffifo_5fmode',['SPI1_FIFO_MODE',['../_s_p_i_8h.html#a1d2e9e0c44be2d464eec745f20316192',1,'SPI.h']]],
  ['spi1_5ffirst_5fbit',['SPI1_FIRST_BIT',['../_s_p_i_8h.html#aedb9093c1539b0c95b0ef156eb782846',1,'SPI.h']]],
  ['spi1_5fmiso',['SPI1_MISO',['../_s_p_i_8h.html#a7bb1fce9599f7a1568a075cd975c6502',1,'SPI.h']]],
  ['spi1_5fmiso_5fmux',['SPI1_MISO_MUX',['../_s_p_i_8h.html#adea04ef4eeb76f4723c16512583467b8',1,'SPI.h']]],
  ['spi1_5fmiso_5fport',['SPI1_MISO_PORT',['../_s_p_i_8h.html#ab5b19097bd266c7ae535367e8ad26b9d',1,'SPI.h']]],
  ['spi1_5fmosi',['SPI1_MOSI',['../_s_p_i_8h.html#a161081b41465c86e1a771c4194041341',1,'SPI.h']]],
  ['spi1_5fmosi_5fmux',['SPI1_MOSI_MUX',['../_s_p_i_8h.html#ac6566a6c703ce04d2520624a30aa2c01',1,'SPI.h']]],
  ['spi1_5fmosi_5fport',['SPI1_MOSI_PORT',['../_s_p_i_8h.html#abd943e6739a8907e4114a1438de0101e',1,'SPI.h']]],
  ['spi1_5fms_5fmode',['SPI1_MS_MODE',['../_s_p_i_8h.html#a551cc9b06688e992a3e0117649f60284',1,'SPI.h']]],
  ['spi1_5fpcs1',['SPI1_PCS1',['../_s_p_i_8h.html#a63779ab870c3d1e3dff563c603f7d95f',1,'SPI.h']]],
  ['spi1_5fpcs1_5fmux',['SPI1_PCS1_MUX',['../_s_p_i_8h.html#ae42cb92324b7d10889227c1a92a695bf',1,'SPI.h']]],
  ['spi1_5fpcs1_5fport',['SPI1_PCS1_PORT',['../_s_p_i_8h.html#ade4c1e23c118695219c0d420ac92a049',1,'SPI.h']]],
  ['spi1_5fread_5fbyte',['spi1_read_byte',['../_s_p_i_8c.html#a37e7b7259c94fb6cec781a49c2f39e96',1,'spi1_read_byte(uint8_t data):&#160;SPI.c'],['../_s_p_i_8h.html#a37e7b7259c94fb6cec781a49c2f39e96',1,'spi1_read_byte(uint8_t data):&#160;SPI.c']]],
  ['spi1_5freceive_5fbuffer',['spi1_receive_buffer',['../_s_p_i_8c.html#a094d036b7e71611e147b72c8868e543e',1,'spi1_receive_buffer():&#160;SPI.c'],['../_s_p_i_8h.html#a094d036b7e71611e147b72c8868e543e',1,'spi1_receive_buffer():&#160;SPI.c']]],
  ['spi1_5freceive_5fbuffer_5fcounter',['spi1_receive_buffer_counter',['../_s_p_i_8c.html#a8a3dd92870c8e89ae7936ef6fc3338e6',1,'spi1_receive_buffer_counter():&#160;SPI.c'],['../_s_p_i_8h.html#a8a3dd92870c8e89ae7936ef6fc3338e6',1,'spi1_receive_buffer_counter():&#160;SPI.c']]],
  ['spi1_5freceive_5fbuffer_5fsize',['spi1_receive_buffer_size',['../_s_p_i_8c.html#a60a60d88a808e74a2576d1428af60960',1,'spi1_receive_buffer_size():&#160;SPI.c'],['../_s_p_i_8h.html#a60a60d88a808e74a2576d1428af60960',1,'spi1_receive_buffer_size():&#160;SPI.c']]],
  ['spi1_5frecive_5fmode',['spi1_recive_mode',['../_s_p_i_8c.html#a89732086e2c65340b07fca90d68e2a75',1,'SPI.c']]],
  ['spi1_5fsck',['SPI1_SCK',['../_s_p_i_8h.html#a241b59557c3e3c91418162ed4d682aaa',1,'SPI.h']]],
  ['spi1_5fsck_5fmux',['SPI1_SCK_MUX',['../_s_p_i_8h.html#aa890aa7e2d5b634d1fc6186d77d572bd',1,'SPI.h']]],
  ['spi1_5fsck_5fport',['SPI1_SCK_PORT',['../_s_p_i_8h.html#a16c59789b6e89795dedda18d344950b3',1,'SPI.h']]],
  ['spi1_5fset_5freceive_5fbuffer',['spi1_set_receive_buffer',['../_s_p_i_8h.html#ad490a357f3303054e79c29e8b3efb9e9',1,'SPI.h']]],
  ['spi1_5fsppr',['SPI1_SPPR',['../_s_p_i_8h.html#a78bab10865a881a97ddd05f7343e9b32',1,'SPI.h']]],
  ['spi1_5fspr',['SPI1_SPR',['../_s_p_i_8h.html#ae56abe401a7a257e6f5fcf9b1593f2a9',1,'SPI.h']]],
  ['spi1_5ftransfer_5fmode',['SPI1_TRANSFER_MODE',['../_s_p_i_8h.html#a45500047d062256853102efddd9d0042',1,'SPI.h']]],
  ['spi1init',['spi1init',['../_s_p_i_8c.html#aa80c9e7e30cd4581c56634296f1305e4',1,'spi1init():&#160;SPI.c'],['../_s_p_i_8h.html#a8d911e61a340baa8d2a7e83c3cf76e4f',1,'spi1init(void):&#160;SPI.c']]],
  ['sport',['SPORT',['../_s_p_i_8h.html#a1b5a31b238724b3b37230067f89cb964',1,'SPI.h']]],
  ['sspi',['SSPI',['../_s_p_i_8h.html#aaebfc68e96489d76198f995425026b10',1,'SPI.h']]],
  ['status',['STATUS',['../n_r_f24_l01_p_8h.html#a59279bee44f34d08b3cbf3a89fb0d8d9',1,'STATUS():&#160;nRF24L01P.h'],['../n_r_f24_l01_p__old_8h.html#a59279bee44f34d08b3cbf3a89fb0d8d9',1,'STATUS():&#160;nRF24L01P_old.h']]]
];
