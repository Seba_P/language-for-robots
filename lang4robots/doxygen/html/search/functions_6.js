var searchData=
[
  ['slcdclear',['slcdClear',['../slcd_8c.html#aaaff4e04850c66e370264dc74d0964cc',1,'slcdClear(void):&#160;slcd.c'],['../slcd_8h.html#aaaff4e04850c66e370264dc74d0964cc',1,'slcdClear(void):&#160;slcd.c']]],
  ['slcddemo',['slcdDemo',['../slcd_8c.html#ae2a224bcd35d271a7ea6615efc71b144',1,'slcdDemo(void):&#160;slcd.c'],['../slcd_8h.html#ae2a224bcd35d271a7ea6615efc71b144',1,'slcdDemo(void):&#160;slcd.c']]],
  ['slcddisplay',['slcdDisplay',['../slcd_8c.html#ace28f520bc05c0f3f359dffa31d9ffe9',1,'slcdDisplay(uint16_t value, uint16_t format):&#160;slcd.c'],['../slcd_8h.html#ace28f520bc05c0f3f359dffa31d9ffe9',1,'slcdDisplay(uint16_t value, uint16_t format):&#160;slcd.c']]],
  ['slcderr',['slcdErr',['../slcd_8c.html#a9a69ca2f6badebd7740c836eef1d2b56',1,'slcdErr(uint8_t number):&#160;slcd.c'],['../slcd_8h.html#a9a69ca2f6badebd7740c836eef1d2b56',1,'slcdErr(uint8_t number):&#160;slcd.c']]],
  ['slcdinitialize',['slcdInitialize',['../slcd_8c.html#a2b629aa8c70eb086a7a4e61a8d074f13',1,'slcdInitialize(void):&#160;slcd.c'],['../slcd_8h.html#a2b629aa8c70eb086a7a4e61a8d074f13',1,'slcdInitialize(void):&#160;slcd.c']]],
  ['slcdset',['slcdSet',['../slcd_8c.html#a6c8dfaa32f3473330d766defea9a51fd',1,'slcdSet(uint8_t value, uint8_t digit):&#160;slcd.c'],['../slcd_8h.html#a6c8dfaa32f3473330d766defea9a51fd',1,'slcdSet(uint8_t value, uint8_t digit):&#160;slcd.c']]],
  ['spi0_5fbyte_5fsend',['spi0_byte_send',['../_s_p_i_8h.html#aeb619a69d7a7df5cefa9f3da353244e5',1,'SPI.h']]],
  ['spi0_5fset_5fmatch_5fvalue',['spi0_set_match_value',['../_s_p_i_8c.html#abc9574979e282bfab80ddac4d50e5a2e',1,'spi0_set_match_value(uint8_t ML, uint8_t MH):&#160;SPI.c'],['../_s_p_i_8h.html#abc9574979e282bfab80ddac4d50e5a2e',1,'spi0_set_match_value(uint8_t ML, uint8_t MH):&#160;SPI.c']]],
  ['spi0_5fset_5fmatch_5fvalue_5fml',['spi0_set_match_value_ML',['../_s_p_i_8c.html#a778d0cfec30eee9b8790e6f180abc66c',1,'spi0_set_match_value_ML(uint8_t ML):&#160;SPI.c'],['../_s_p_i_8h.html#a778d0cfec30eee9b8790e6f180abc66c',1,'spi0_set_match_value_ML(uint8_t ML):&#160;SPI.c']]],
  ['spi0init',['spi0init',['../_s_p_i_8h.html#a3a755190f64d26b666d115e30439caea',1,'SPI.h']]],
  ['spi1_5fbyte_5fsend',['spi1_byte_send',['../_s_p_i_8c.html#a6689cb079c9e7a6ea2b4d7968ae4029b',1,'spi1_byte_send(uint8_t data, _Bool receive):&#160;SPI.c'],['../_s_p_i_8h.html#a6689cb079c9e7a6ea2b4d7968ae4029b',1,'spi1_byte_send(uint8_t data, _Bool receive):&#160;SPI.c']]],
  ['spi1_5fdata_5fsend',['spi1_data_send',['../_s_p_i_8h.html#ab8ae59cdc1b359e7fcba46f406543cde',1,'SPI.h']]],
  ['spi1_5fread_5fbyte',['spi1_read_byte',['../_s_p_i_8c.html#a37e7b7259c94fb6cec781a49c2f39e96',1,'spi1_read_byte(uint8_t data):&#160;SPI.c'],['../_s_p_i_8h.html#a37e7b7259c94fb6cec781a49c2f39e96',1,'spi1_read_byte(uint8_t data):&#160;SPI.c']]],
  ['spi1_5fset_5freceive_5fbuffer',['spi1_set_receive_buffer',['../_s_p_i_8h.html#ad490a357f3303054e79c29e8b3efb9e9',1,'SPI.h']]],
  ['spi1init',['spi1init',['../_s_p_i_8c.html#aa80c9e7e30cd4581c56634296f1305e4',1,'spi1init():&#160;SPI.c'],['../_s_p_i_8h.html#a8d911e61a340baa8d2a7e83c3cf76e4f',1,'spi1init(void):&#160;SPI.c']]]
];
